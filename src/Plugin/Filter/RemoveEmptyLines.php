<?php

namespace Drupal\cbeier_drupal_toolkit\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter for markdown.
 *
 * @Filter(
 *   id = "remove_empty_lines",
 *   title = @Translation("Remove empty lines"),
 *   description = @Translation("Modifies the html and removes all empty lines (e.g. multiple <br> behind each other)"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class RemoveEmptyLines extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (!empty($text)) {
      // Remove all breaks for the filtering.
      $text = preg_replace('#\f|\n|\r|\t|\v#', '', $text);

      // Filter constructs like: <p></p>, <p> </p> or <p>&nbsp</p>.
      $text = preg_replace('#<p[^>]*>(&nbsp;|\s)*(</p[^>]*>)#i', '', $text);

      // Filter constructs like: <br /><br /><br />...
      $text = preg_replace('#<br(\s|/)*>((&nbsp;|\s)*<br(\s|/)*>){2,}#i', '<br />', $text);

      // Filter constructs like: <p><br />.
      $text = preg_replace('#<p([^>]*)>(&nbsp;|\s)*<br(\s|/)*>#i', '<p $1>', $text);

      // Filter constructs like: <br /></p>.
      $text = preg_replace('#<br(\s|/)*>(&nbsp;|\s)*</p>+#i', '</p>', $text);

      // Filter &nbsp;.
      $text = str_replace('&nbsp;', ' ', $text);
    }

    return new FilterProcessResult($text);
  }

}
