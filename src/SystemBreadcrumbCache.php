<?php

namespace Drupal\cbeier_drupal_toolkit;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SystemBreadcrumbCache
 *
 * This class/services is used inside a hook_preprocess_breadcrumb() function.
 *
 * The main purpose of this class is to set the necessary cache tags entities
 * that are referenced in the breadcrumb. So if one of this items changes,
 * the breadcrumb can take it into account.
 *
 * @see cbeier_drupal_toolkit_preprocess_breadcrumb().
 *
 * @package Drupal\cbeier_drupal_toolkit
 */
class SystemBreadcrumbCache {

  protected $router;
  protected $routeMatch;
  protected $variables;
  protected $routeEntityItems;

  /**
   * SystemBreadcrumbCache constructor.
   * @param \Symfony\Component\Routing\RouterInterface $router
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   */
  public function __construct(RouterInterface $router, RouteMatchInterface $route_match) {
    $this->router = $router;
    $this->routeMatch = $route_match;
  }

  /**
   * Process.
   *
   * @param $variables
   *   The variables array from the preprocess function.
   *   The variable is used as a variable by reference.
   */
  public function process(&$variables) {
    $this->variables =& $variables;

    $this->setParentRouteEntityItems();
    $this->setCurrentRouteEntityItem();

    foreach ($this->routeEntityItems as $item) {
      $entity_type = $item->getEntityTypeId();
      $entity_id = $item->id();

      $this->setCacheTag($entity_type . ':' . $entity_id);
    }
  }

  /**
   * Set the entities of the parent routes in the breadcrumb.
   */
  protected function setParentRouteEntityItems() {
    foreach ($this->variables['links'] as $link) {
      $url = $link->getUrl();
      $route = $this->router->match($url->toString());

      foreach ($route as $key => $value) {
        if ($value instanceof EntityInterface) {
          $this->routeEntityItems[] = $value;
          break;
        }
      }
    }
  }

  /**
   * Set the entity of the current route.
   */
  protected function setCurrentRouteEntityItem() {
    foreach ($this->routeMatch->getParameters() as $key => $parameter) {
      if ($parameter instanceof EntityInterface) {
        $this->routeEntityItems[] = $parameter;
        return;
      }
    }
  }

  /**
   * Set the given cache tag to the #cache array.
   *
   * @param $tag
   *   The cache tag (e.g. node:1).
   *   https://www.drupal.org/docs/8/api/cache-api/cache-tags
   */
  protected function setCacheTag($tag) {
    $this->variables['#cache']['tags'][] = $tag;
  }

}
