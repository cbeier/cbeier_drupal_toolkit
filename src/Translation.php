<?php

namespace Drupal\cbeier_drupal_toolkit;

use Drupal\locale\SourceString;

class Translation {

  /**
   * Add or update multiple translations at once.
   *
   * @param $langcode
   *   The langcode. All translations must be for the same language.
   * @param array $translations
   *   An keyed array with translations.
   *   The key is the source and the value the translation.
   * @throws \Exception
   */
  public function addMultipleTranslations($langcode, $translations) {
    if (!is_array($translations)) {
      throw new \Exception('Translations needs to be an array.');
    }

    foreach ($translations as $source => $translation) {
      $this->addTranslation($langcode, $source, $translation);
    }
  }

  /**
   * Add or update a single translation string.
   *
   * @param $langcode
   *   The langcode for the translation.
   * @param $source
   *   The source string.
   * @param $translation
   *   The translation string.
   * @param $context
   *   The translation context (optional).
   * @return mixed
   * @throws \Drupal\locale\StringStorageException
   */
  public function addTranslation($langcode, $source, $translation, $context = '') {
    // Find existing source string.
    $storage = \Drupal::service('locale.storage');
    $string = $storage->findString(['source' => $source]);

    if (is_null($string)) {
      $string = new SourceString();
      $string->setString($source);
      $string->setStorage($storage);
      $string->save();
    }

    $translation_options = [
      'lid' => $string->lid,
      'language' => $langcode,
      'source' => $source,
      'translation' => $translation,
    ];

    if (!empty($context)) {
      $translation_options['context'] = $context;
    }

    // Create translation. If one already exists, it will be replaced.
    $translation = $storage->createTranslation($translation_options)->save();

    return $translation;
  }

}
